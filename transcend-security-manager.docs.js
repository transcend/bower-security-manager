/**
 * @ngdoc overview
 * @name transcend.security-manager
 * @description
 # Security Manager Module
 The "Security Manager" module provides all the necessary components to support managing users, roles, data access, etc.
 This module builds on top of the {@link transcend.security Security Module}, which provides the controls to enable
 security in an application.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-security-manager.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-security-manager/get/master.zip](http://code.tsstools.com/bower-security-manager/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-security-manager](http://code.tsstools.com/bower-security-manager)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/security-manager/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.security-manager']);
 ```

 ## Configuration
 You can configure the {@link transcend.security-manager Security Manager Module} by using the standard $provider configuration method
 or by overriding the "{@link transcend.security-manager.securityManagerConfig securityManagerConfig}" object. This object is passed into each component in the
 module and will use any properties set on the object before deferring to the default settings/configuration.

 ```
 angular.module('myApp').value('securityManagerConfig', {
   resource: {
     // Override the default URL for all resources in this module.
     url: 'http://locahost/MyWebApiApp'
   }
 });
 ```

 ## Usage
 By default, the {@link transcend.security-manager Security Manager Module} is configured to work out of the box with an MVC5 WebAPI2
 back-end - with individual accounts setup (using Bearer token). For additional information on setting up the backend
 project to support this front-end control, see the "MVC5 and WebAPI Authentication" section of the
 {@link development/resources Resources Page}.

 ## To Do
 - Add additional components for registration, password change, and role management.

 ## Module Goals
 - Keep the minified module under 15KB.
 */
/**
 * @ngdoc object
 * @name transcend.security-manager.securityManagerConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.security-manager Security Manager Module}. The "securityManagerConfig" object is a
 * {@link http://docs.angularjs.org/api/auto/object/$provide#value module value} and can therefore be
 * overridden/configured by:
 *
 <pre>
 // Override the value of the 'securityManagerConfig' object.
 angular.module('myApp').value('securityManagerConfig', {
     resource: {
       url: 'http://localhost/MyWebApiApp'
     }
   });
 </pre>
 **/
/**
 * @ngdoc object
 * @name transcend.security-manager.securityManagerConfig#resource
 * @propertyOf transcend.security-manager.securityManagerConfig
 *
 * @description
 * Configuration object to configure common resource properties, such as the server URL and default parameters.
 * All resources in the "{@link transcend.security-manager Security Module}" extend the resource config object first, and
 * then extend the resource's specific configuration. So you can set the default backend server URL for all
 * resources, by using this configuration, and define any other configurations that may vary as well.
 *
 * ## Configuration Properties
 *   * __url__ - a string that that represents the base url to the backend server.
 *   * __params__ - default URL parameters that will be passed to each service call.
 *
 <pre>
 angular.module('myApp').value('securityManagerConfig', {
       // The "Account" resource's URL will be set
       // by the resource url configuration.
       resource: {
         url: 'http://localhost/MyWebApiApplication'
       },
       // The "Role" resource's URL will initially be set by
       // the resource url configuration, but
       // will be overridden by the "role" configuration.
       // So ultimately the "Role" resource URL will
       // be 'http://otherhost/MyOtherApplication'.
       role: {
         url: 'http://otherhost/MyOtherApplication'
       }
     });
 </pre>
 */
/**
 * @ngdoc directive
 * @name transcend.security-manager.directive:siteSecurityManager
 *
 * @description
 * The 'siteSecurityManager' directive provides the ability to see and edit the site's security/authentication mechanism.
 *
 * @restrict AE
 * @element ANY
 *
 * @requires SecurityProfile
 * @requires transcend.core.$notify
 * @requires $array
 *
 */
/**
 * @ngdoc service
 * @name transcend.security-manager:SecurityProfile
 *
 * @description
 * The 'SecurityProfile' factory returns a new Vector Editor for SVG purposees.
 *
 * @requires $resource
 * @requires $q
 * @requires securityManagerConfig
 *
 */
/**
 * @ngdoc service
 * @name transcend.security-manager.service:DataLayerPrivilege
 *
 * @description
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} to interact with a "DataLayerPrivilege" entity.
 * A "DataLayerPrivilege" entity models a single layer privilege's visible and editable layers. The purpose of this
 * {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} is to provide the necessary
 * functionality to interact with a "DataLayerPrivilege" - such as create, read, update, and delete.
 *
 <pre>
 // Get a list of data layer privileges.
 var dataLayerPrivileges = DataLayerPrivilege.query();

 // Get a single data layer privilege by id.
 var dataLayerPrivilege = DataLayerPrivilege.get({ id: abc });

 // Update a data layer privilege.
 dataLayerPrivilege.visibleLayers = [1,2,3];
 DataLayerPrivilege.update(dataLayerPrivilege);

 // Delete a data layer privilege.
 DataLayerPrivilege.delete(dataLayerPrivilege);
 </pre>
 *
 * @requires $resource
 * @requires transcend.security-manager.securityManagerConfig
 */
/**
     * @ngdoc object
     * @name transcend.security-manager.securityManagerConfig#role
     * @propertyOf transcend.security-manager.securityManagerConfig
     *
     * @description
     * Configuration object for the {@link transcend.security-manager.service:Role Role resource}. This object provides
     * the capability to override the the {@link transcend.security-manager.service:Role Role resource's} default
     * configuration.
     *
     * ## Configuration Properties
     *   * __url__ - a string that that represents the base url to the backend server.
     *
     <pre>
     angular.module('myApp').value('securityManagerConfig', {
       role: {
         url: 'http://localhost/MyWebApiApplication'
       }
     });
     </pre>
     *
     * __Note,__ all methods on a resource can be called on the resource instance by prefixing the method name with a
     * '$'. This is standard {@link transcend.security.service:Account resource} behaviour.
     *
     * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
     * details on what methods are natively available to a resource.
     */
/**
 * @ngdoc directive
 * @name transcend.security-manager.directive:dataLayerPrivilegeManager
 *
 * @description
 * The 'roleFinder' directive provides an HTML element that can be used to display and select a role returned from the the Role service. The directive
 * will utilize a dropdown select box to display the current roles.
 *
 * @restrict EA
 * @element ANY
 *
 */
/**
 * @ngdoc service
 * @name transcend.security-manager.service:Privilege
 *
 * @description
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} to interact with a "Role" entity.
 * A "Role" entity models a single role. The purpose of this
 * {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} is to provide the necessary
 * functionality to interact with a "Role" - such as create, read, update, and delete.
 *
 <pre>
 // Get a list of privileges.
 var privileges = Privilege.query();

 // Get a single role by id.
 var privilege = Privilege.get({ id: 4 });

 // Update a role.
 privilege.description = 'Some description...'
 privilege.$save();

 // Delete a role.
 privilege.$delete();
 </pre>
 *
 * @requires $resource
 * @requires transcend.security-manager.securityManagerConfig
 */
/**
     * @ngdoc object
     * @name transcend.security-manager.securityManagerConfig#privilege
     * @propertyOf transcend.security-manager.securityManagerConfig
     *
     * @description
     * Configuration object for the {@link transcend.security-manager.service:Privilege Privilege resource}. This object
     * provides the capability to override the the {@link transcend.security-manager.service:Privilege Privilege resource's}
     * default configuration.
     *
     * ## Configuration Properties
     *   * __url__ - a string that that represents the base url to the backend server.
     *
     <pre>
     angular.module('myApp').value('securityManagerConfig', {
       privilege: {
         url: 'http://localhost/MyWebApiApplication'
       }
     });
     </pre>
     *
     * __Note,__ all methods on a resource can be called on the resource instance by prefixing the method name with a
     * '$'. This is standard {@link transcend.security.service:Account resource} behaviour.
     *
     * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
     * details on what methods are natively available to a resource.
     */
/**
 * @ngdoc directive
 * @name transcend.security-manager.directive:roleFinder
 *
 * @description
 * The 'roleFinder' directive provides an HTML element that can be used to display and select a role returned from the the Role service. The directive
 * will utilize a dropdown select box to display the current roles.
 *
 * @restrict EA
 * @element ANY
 *
 */
/**
 * @ngdoc directive
 * @name transcend.security-manager.directive:roleEditor
 *
 * @description
 * The 'roleFinder' directive provides an HTML element that can be used to display and select a role returned from the the Role service. The directive
 * will utilize a dropdown select box to display the current roles.
 *
 * @restrict EA
 * @element ANY
 *
 */
/**
 * @ngdoc service
 * @name transcend.security-manager.service:RolePrivilege
 *
 * @description
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} to interact with a "RolePrivilege" entity.
 * A "RolePrivilege" entity models a single role to privilege relationship. The purpose of this
 * {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} is to provide the necessary
 * functionality to interact with a "RolePrivilege" - such as create, read, update, and delete.
 *
 <pre>
 // Get a list of roles.
 var rolePrivs = RolePrivilege.query();
 </pre>
 *
 * @requires $resource
 * @requires transcend.security-manager.securityManagerConfig
 */
/**
     * @ngdoc object
     * @name transcend.security-manager.securityManagerConfig#rolPrivilege
     * @propertyOf transcend.security-manager.securityManagerConfig
     *
     * @description
     * Configuration object for the {@link transcend.security-manager.service:RolePrivilege RolePrivilege resource}.
     * This object provides the capability to override the the {@link transcend.security-manager.service:RolePrivilege RolePrivilege resource's}
     * default configuration.
     *
     * ## Configuration Properties
     *   * __url__ - a string that that represents the base url to the backend server.
     *
     <pre>
     angular.module('myApp').value('securityManagerConfig', {
       rolePrivilege: {
         url: 'http://localhost/MyWebApiApplication'
       }
     });
     </pre>
     *
     * __Note,__ all methods on a resource can be called on the resource instance by prefixing the method name with a
     * '$'. This is standard {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} behaviour.
     *
     * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
     * details on what methods are natively available to a resource.
     */
/**
 * @ngdoc directive
 * @name transcend.security-manager.directive:rolePrivileges
 *
 * @description
 * The 'rolePrivileges' directive provides an HTML element that provides an interface to view and edit the privileges assigned to a role. This directive utilizes the
 * 'roleFinder' directive to display a dropdown list of current roles. The 'rolePriviliges' directive requires the Role, Privilege and RolePrivilege services to view and edit
 * role privileges.
 *
 *
 * @restrict EA
 * @element ANY
 */
/**
 * @ngdoc service
 * @name transcend.security-manager.service:Role
 *
 * @description
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} to interact with a "Role" entity.
 * A "Role" entity models a single role. The purpose of this
 * {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} is to provide the necessary
 * functionality to interact with a "Role" - such as create, read, update, and delete.
 *
 <pre>
 // Get a list of roles.
 var roles = Role.query();

 // Get a single role by id.
 var role = Role.get({ id: abc });

 // Update a role.
 role.description = 'Some description...'
 Role.update(role);

 // Delete a role.
 Role.delete(role);
 </pre>
 *
 * @requires $resource
 * @requires transcend.security-manager.securityManagerConfig
 */
/**
     * @ngdoc object
     * @name transcend.security-manager.securityManagerConfig#role
     * @propertyOf transcend.security-manager.securityManagerConfig
     *
     * @description
     * Configuration object for the {@link transcend.security-manager.service:Role Role resource}. This object provides
     * the capability to override the the {@link transcend.security-manager.service:Role Role resource's} default
     * configuration.
     *
     * ## Configuration Properties
     *   * __url__ - a string that that represents the base url to the backend server.
     *
     <pre>
     angular.module('myApp').value('securityManagerConfig', {
       role: {
         url: 'http://localhost/MyWebApiApplication'
       }
     });
     </pre>
     *
     * __Note,__ all methods on a resource can be called on the resource instance by prefixing the method name with a
     * '$'. This is standard {@link transcend.security.service:Account resource} behaviour.
     *
     * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
     * details on what methods are natively available to a resource.
     */
/**
 * @ngdoc service
 * @name transcend.security-manager.service:UserRole
 *
 * @description
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} to interact with a "UserRole" entity.
 * A "UserRole" entity models a single role to user relationship. The purpose of this
 * {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} is to provide the necessary
 * functionality to interact with a "UserRole" - such as create, read, update, and delete.
 *
 <pre>
 // Get a list of roles.
 var userRoles = UserRole.query({ userId: '123' });
 </pre>
 *
 * @requires $resource
 * @requires transcend.security-manager.securityManagerConfig
 */
/**
 * @ngdoc directive
 * @name transcend.security-manager.directive:userRolesManager
 *
 * @description
 * The 'userRolesManager' directive provides an HTML element that can be used to display and edit the roles assigned to
 * a user. The directive includes the userFinder directive for user lookup. The 'userRolesManager' utilizes the User,
 * Role, and UserRole services to retrieve and update users and their roles.
 *
 * @restrict EA
 * @element ANY
 */
