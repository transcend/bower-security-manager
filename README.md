# Security Manager Module
The "Security Manager" module provides all the necessary components to support managing users, roles, data access, etc.
This module builds on top of the Security Module, which provides the controls to enable security in an application.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-security-manager.git
```

Other options:

*   * Download the code at [http://code.tsstools.com/bower-security-manager/get/master.zip](http://code.tsstools.com/bower-security-manager/get/master.zip)
*   * View the repository at [http://code.tsstools.com/bower-security-manager](http://code.tsstools.com/bower-security-manager)
*   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/security-manager/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.security-manager']);
```

## Configuration
You can configure the [http://docs.tsstools.com/#/api/transcend.security-manager](Security Manager Module) by using the standard $provider configuration method
or by overriding the "[http://docs.tsstools.com/#/api/transcend.security-manager.securityManagerConfig](securityManagerConfig)" object. This object is passed into each component in the
module and will use any properties set on the object before deferring to the default settings/configuration.

```
angular.module('myApp').value('securityManagerConfig', {
resource: {
 // Override the default URL for all resources in this module.
 url: 'http://locahost/MyWebApiApp'
}
});
```

## Usage
By default, the [http://docs.tsstools.com/#/api/transcend.security-manager](Security Manager Module) is configured to
back-end - with individual accounts setup (using Bearer token). For additional information on setting up the backend
project to support this front-end control, see the "MVC5 and WebAPI Authentication" section of the
[Resources Page](http://docs.tsstools.com/#/development/resources).

## To Do
- Finish unit tests.

## Module Goals
- Keep the minified module under 15KB.